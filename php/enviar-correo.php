<?php
session_start();
$httpProtocol = 'https://';
$host = $_SERVER['SERVER_NAME'];
$url = '/fasst2/';

if(isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["tel"]) && isset($_POST["message"]) && isset($_POST["area"])){
    include("../mail/class.phpmailer.php");
    include("../mail/class.smtp.php");
	
	$lang = $_POST["lang"];
    $area = $_POST["area"];
    $nombre = $_POST["name"];
    $email = $_POST["email"];
    $telefono = $_POST["tel"];
    $mensaje = $_POST["message"];
    $contenido = "";
    
    $contenido .= "<html>
			<head>
				<meta charset='UTF-8'>
				<style>
					*{
						font-family: Arial;
					}
					td{
						border: 1px solid #ddd;
					}
					table{
						background: #f1f1f1;
						border-collapse: collapse;
					}
				</style>
			</head>
			<body>
				<table width='600' align='center' valign='center' cellpadding='10'>
					<tr style='background-color: gray;'>
						<td colspan='2'><img src='".$httpProtocol."".$host."img/fasst_logo_header.png'></td>
					</tr>
					<tr>
						<td>Enviado desde: </td><td>".$_SERVER['HTTP_REFERER']."</td>
					</tr>
                    <tr>
						<td>Nombre: </td><td>".$nombre."</td>
					</tr>
					<tr>
						<td>Correo: </td><td>".$email."</td>
					</tr>
					<tr>
						<td>Teléfono: </td><td>".$telefono."</td>
					</tr>
                    <tr>
						<td>Área de Interés: </td><td>".$area."</td>
					</tr>
                    <tr>
						<td>Mensaje: </td><td>".$mensaje."</td>
					</tr>
				</table>
			</body>
		</html>";
    
    $mail = new PHPMailer(); 
    $mail->From = "info@grupofasst.com";
	$mail->FromName = "Formulario de contacto web";
	/* $mail->AddAddress("jgonzalez@callfasst.com");
	$mail->AddBCC("llopez@fasst.com.mx");
	$mail->AddBCC("aleal@bitaminadigital.com"); */
    $mail->AddAddress("aleal@bitaminadigital.com");
    //$mail->AddBCC("");
    $mail->IsHTML(true);
    $mail->Subject = "Correo enviado desde sitio web";
    $mail->Body = $contenido;
    
    if($mail->Send()){
	   if($lang == "EN"){
			header('Location: '.$httpProtocol.$host.$url.'en/successfull-delivery.php');
	   }else{
			header('Location: '.$httpProtocol.$host.$url.'correo-enviado.php');
	   }
    }
}else{
    $_SESSION["error"] = 1;
	header('Location: '.$_SERVER['HTTP_REFERER']);
}
?>