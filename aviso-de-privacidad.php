<!DOCTYPE html>
<?php  
    $title = 'Aviso de privacidad';
	$description = 'Aviso de Privacidad Fasst IT Specialist';
	$keywords = 'aviso de privacidad fasst it';
    $author = 'Bitamina Digital';
    $lang = 'es';
    
    $page = 'aviso';
    include('commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'aviso-de-privacidad'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/privacy-notice'.$ext;
    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/aviso-privacidad.html');
    include('views/modal-contacto.html');
    include('views/footer.html');
?>