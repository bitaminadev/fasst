<!DOCTYPE html>
<?php  
    $title = 'Correo enviado';
	$description = 'Correo enviado';
	$keywords = '';
    $author = 'Bitamina Digital';
    $lang = 'es';
    
    $page = 'aviso';
    include('commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'correo-enviado'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/successfull-delivery'.$ext;
    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/correo-enviado.html');
    include('views/modal-contacto.html');
    include('views/footer.html');
?>