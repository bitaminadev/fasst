<!DOCTYPE html>
<?php  
    $title = 'Servicios de Reclutamiento y selección de personal';
	$description = 'En Fasst damos soluciones para el reclutamiento, selección y contratación de personal en Querétaro y San Luis Potosí, a través de QRE.';
	$keywords = 'fasst, qre, reclutamiento de personal, selección de personal, contratación de personal';
    $author = 'Bitamina Digital';
    $lang = 'es';
    
    $page = 'reclutamiento';
    include('commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'reclutamiento-y-seleccion'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/recruiment'.$ext;
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesReclutamiento.css">';

    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/reclutamiento.html');
    include('views/modal-contacto.html');
    include('views/footer.html');
?>