<!DOCTYPE html>
<?php  
    $title = 'Geolocation and Telemetry Services';
	$description = 'Geolocation and telemetry solutions for people and transportation services through Ubiqo and Evidence.';
	$keywords = 'fasst, ubiqo, evidence, geolocation, telemetry';
    $author = 'Bitamina Digital';
    $lang = 'en';
    
    $page = 'geolocalizacion';
    include('../commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'geolocalizacion-y-telemetria'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/geolocation-and-telemetric'.$ext;
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesGeolocalizacion.css">';

    include('../commons/_headClose.php');
    include('../views/en/navbar.html');
    include('../views/en/geolocalizacion.html');
    include('../views/en/modal-contacto.html');
    include('../views/en/footer.html');
?>