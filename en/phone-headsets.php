<!DOCTYPE html>
<?php  
    $title = 'Professional Phone Headsets for Call and Contact Centers';
	$description = 'Communication solutions with Accutone phone headsets for call & contact center operation in Mexico and Latin America.';
	$keywords = 'fasst, accutone, phone headsets, call center, contact center';
    $author = 'Bitamina Digital';
    $lang = 'en';
    
    $page = 'diademas';
    include('../commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'diademas-telefonicas'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/phone-headsets'.$ext;
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesDiademas.css">';

    include('../commons/_headClose.php');
    include('../views/en/navbar.html');
    include('../views/en/diademas.html');
    include('../views/en/modal-contacto.html');
    include('../views/en/footer.html');
?>