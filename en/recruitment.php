<!DOCTYPE html>
<?php  
    $title = 'Personnel Recruitment and Selection Services';
	$description = 'Solutions for staff rectruitment and selection in Querétaro and San Luis Potosí through QRE.';
	$keywords = 'fasst, qre, staff recruitment, personnel selection, staff hiring';
    $author = 'Bitamina Digital';
    $lang = 'en';
    
    $page = 'reclutamiento';
    include('../commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'reclutamiento-y-seleccion'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/recruiment'.$ext;
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesReclutamiento.css">';

    include('../commons/_headClose.php');
    include('../views/en/navbar.html');
    include('../views/en/reclutamiento.html');
    include('../views/en/modal-contacto.html');
    include('../views/en/footer.html');
?>