<!DOCTYPE html>
<?php  
    $title = 'Digital Marketing Services in Mexico';
	$description = 'Digital marketing solutions through Bitamina Digital; SEM, SEO, social media and web design.';
	$keywords = 'fasst, bitamina digital, seo, sem, web design, social media';
    $author = 'Bitamina Digital';
    $lang = 'en';
    
    $page = 'marketing';
    include('../commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'marketing-digital'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/digital-marketing'.$ext;
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesMarketing.css">';

    include('../commons/_headClose.php');
    include('../views/en/navbar.html');
    include('../views/en/marketing.html');
    include('../views/en/modal-contacto.html');
    include('../views/en/footer.html');
?>