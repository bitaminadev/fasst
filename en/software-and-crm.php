<!DOCTYPE html>
<?php  
    $title = 'CRM and Software Development in Mexico';
	$description = 'CRM and software development for recruitment firms and schools: Prospekta';
	$keywords = 'fasst, kapta, crm schools, prospekta, software development, crm';
    $author = 'Bitamina Digital';
    $lang = 'en';
    
    $page = 'software';
    include('../commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'software-y-crm'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/software-and-crm'.$ext;
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesSoftware.css">';

    include('../commons/_headClose.php');
    include('../views/en/navbar.html');
    include('../views/en/software.html');
    include('../views/en/modal-contacto.html');
    include('../views/en/footer.html');
?>