<!DOCTYPE html>
<?php  
    $title = 'Fasst Group: Communication and IT Leading Company';
	$description = 'Communication and IT solutions for establishing brands and innovation in Mexico and Latin America.';
	$keywords = 'fasst group, it company, communication, Mexico';
    $author = 'Bitamina Digital';
    $lang = 'en';
    
    $page = 'fasst';
    include('../commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'grupo-fasst'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/fasst-group'.$ext;
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesFasst.css">';
    $js .= '
        <script>
        $(document).ready(function() {
            $(".carousel").carousel();
        });
        </script>
    ';
    include('../commons/_headClose.php');
    include('../views/en/navbar.html');
    include('../views/en/fasst.html');
    include('../views/en/modal-contacto.html');
    include('../views/en/footer.html');
?>