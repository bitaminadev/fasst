<!DOCTYPE html>
<?php  
    $title = 'Successfull delivery';
	$description = 'Successfull delivery';
	$keywords = '';
    $author = 'Bitamina Digital';
    $lang = 'en';

    $page = 'home';
    include('../commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'correo-enviado'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/successfull-delivery'.$ext;

    include('../commons/_headClose.php');
    include('../views/en/navbar.html');
    include('../views/en/correo-enviado.html');
    include('../views/en/modal-contacto.html');
    include('../views/en/footer.html');
?>