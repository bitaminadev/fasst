<!DOCTYPE html>
<?php  
    $title = 'Contact Center Services in Mexico';
	$description = 'Inbound and outbound solutions through contact centers in Mexico; CallFasst & Contactum Services.';
	$keywords = 'fasst, contact center, callfasst, contactum, call center';
    $author = 'Bitamina Digital';
    $lang = 'en';

    $page = 'home';
    include('../commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'contact-center'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/contact-center'.$ext;
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesHome.css">';
    
    include('../commons/_headClose.php');
    include('../views/en/navbar.html');
    include('../views/en/contact-center.html');
    include('../views/en/modal-contacto.html');
    include('../views/en/footer.html');
?>