<!DOCTYPE html>
<?php  
    $title = 'Servicios de Marketing Digital en México';
	$description = 'En Fasst damos solución a las marketing digital a través de Bitamina Digital con servicios de diseño web, sem, seo y redes sociales.';
	$keywords = 'Fasst, bitamina digital, seo, sem, diseño web, redes sociales';
    $author = 'Bitamina Digital';
    $lang = 'es';
    
    $page = 'marketing';
    include('commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'marketing-digital'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/digital-marketing'.$ext;
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesMarketing.css">';

    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/marketing.html');
    include('views/modal-contacto.html');
    include('views/footer.html');
?>