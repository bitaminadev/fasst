const regexNumber = /[0-9]{1}/;

$(document).ready(function(){
   $("#tel").on("keypress", justNumbers);
});

function justNumbers(e){
   return regexNumber.test(e.originalEvent.key);
}

function sendEmailContact(){
    // Función para checar que no estén vacíos los campos
    var checkIsNotEmpty = function(auxiliar, name){
        if(auxiliar.value == '' || auxiliar.value == null){
			let msg = 'Por favor completa el campo de: ' + name;
            messageError(msg);
            return false;
		}
        return true;
    }
    // Función para checar si un campo tiene números en su strings
    var checkHasNoNumbers = function(auxiliar, name){
        if(/\d/.test(auxiliar.value)){
            let msg = 'Por favor remueve los números del campo de: ' + name;
            messageError(msg);
            return false;
		}
        return true;
    }
    // Función para checar si un campo tiene letras en su string de puros números
    var checkHasNumbers = function(auxiliar, name){
        if(/[^0-9]/.test(auxiliar.value)){
			let msg = 'Por favor remueve cualquier caracter que no sea número del campo de: ' + name;
            messageError(msg);
            return false;
		}
        return true;
    }
    // Función que agrega el mensaje de error a mostrar
    var messageError = function(msgErr){
        $('#message_item').html(msgErr);
        $('.alert').alert();
        $('.alert').css('z-index','10000');
        $('.alert').css('position','fixed');
        $('.alert').css('bottom','50%');
        $('.alert').css('left','33%');
        $('.alert').css('right','35%');
        $(".alert").show().delay(200).addClass("in").fadeOut(3500);
        $('.alert').on('closed.bs.alert', function () {
            $('.alert').alert('dispose');
        });
    }
    
    // Obtenemos la información guardada en el form de contacto
    var formulario = document.getElementById('form-contacto'),
    name = formulario.name,
    email = formulario.email,
    tel = formulario.tel,
    area = formulario.area,
    message = formulario.message;

    var flag = 0;
    // Checar que ningún campo del form esté vacío o incorrecto
    if(checkIsNotEmpty(name,'Nombre') && checkHasNoNumbers(name,'Nombre')){
        if(checkIsNotEmpty(tel,'Teléfono') && checkHasNumbers(tel,'Teléfono')){
            if(checkIsNotEmpty(email,'Correo Electrónico')){
                if(checkIsNotEmpty(area,'Área de Interés')){
                    if(checkIsNotEmpty(message,'Mensaje')){
                        // Flag para saber si ejecutar o no el envío de formulario    
                        var flag = 1;
                    }
                }
            }
        }
    }
    // Si la bandera es 1 es porque paso todas las validaciones
    if(flag == 1){
        return true;
    }else{
        return false;
    }
}
