<!DOCTYPE html>
<?php  
    $title = 'Desarrollo de Software y Crm en México';
	$description = 'En Fasst desarrollamos software y crm para escuelas y empresas de reclutamiento a través de Kapta CRM Schools y Prospekta.';
	$keywords = 'Fasst, Kapta CRM Shcools, Prospekta, desarrollo de software, crm';
    $author = 'Bitamina Digital';
    $lang = 'es';
    
    $page = 'software';
    include('commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'software-y-crm'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/software-and-crm'.$ext;
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesSoftware.css">';

    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/software.html');
    include('views/modal-contacto.html');
    include('views/footer.html');
?>