<!DOCTYPE html>
<?php  
    $title = 'Grupo Fasst empresa líder en TI y Comunicación';
	$description = 'En Grupo Fasst nos especializamos en dar soluciones de TI y Comunicación a través de la innovación y marcas consolidadas en México y Latinoamérica.';
	$keywords = 'grupo fasst, empresa de TI, Comunicación, México';
    $author = 'Bitamina Digital';
    $lang = 'es';
    
    $page = 'fasst';
    include('commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'grupo-fasst'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/fasst-group'.$ext;

    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesFasst.css">';
    $js .= '
        <script>
        $(document).ready(function() {
            $(".carousel").carousel();
        });
        </script>
    ';
    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/fasst.html');
    include('views/modal-contacto.html');
    include('views/footer.html');
?>