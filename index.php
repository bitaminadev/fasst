<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="es">
<head>
	<!--config inicial-->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 user-scalable=no"/>
	<title>Fasst empresa de tecnología de información y comunicación en Querétaro</title>
	<meta name="description" content = "En Fasst nos especializamos en el desarrollo de soluciones de TI a través de diferentes unidades en desarrollo, plataformas, call center,  software y marketing digital." />
	<meta name= "keywords" content="fasst, ti, call center, tecnología de información, software, marketing digital" />
	
	<meta name="geo.region" content="MX-QUE" />
    <meta name="geo.placename" content="Quer&eacute;taro" />
    <meta name="geo.position" content="20.589103;-100.365155" />
    <meta name="ICBM" content="20.589103, -100.365155" />
	
	<meta property="og:locale" content="es" />
	<meta property="og:title" content="Fasst empresa de tecnología de información y comunicación en Querétaro" />
	<meta property="og:description" content="En Fasst nos especializamos en el desarrollo de soluciones de TI a través de diferentes unidades en desarrollo, plataformas, call center,  software y marketing digital." />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://www.fasst.com.mx/" />
	<meta property="og:image" content="https://www.fasst.com.mx/img/fasst_logo_header.png" />
	<meta property="og:site_name" content="Fasst it specialist" />
	
	<meta name="dc.language" content="es">
	<meta NAME="DC.Title" content="Fasst empresa de tecnología de información y comunicación en Querétaro"/>
	<meta NAME="DC.Description" content="En Fasst nos especializamos en el desarrollo de soluciones de TI a través de diferentes unidades en desarrollo, plataformas, call center,  software y marketing digital."/>
	<meta NAME="DC.Identifier" content="https://www.fasst.com.mx/"/>
	<meta NAME="DC.Creator" content="Bitamina Digital"/>
	<meta NAME="DC.Publisher" content="2019-09-23"/>
	
	<link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
	<link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#5bbad5">

	<link rel="alternate" hreflang="x-default" href="https://www.fasst.com.mx" />
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Teko:300,500,600,700" rel="stylesheet">
	<link href="css/index.css" rel="stylesheet" type="text/css"/>
	<style type="text/css">
		.box {
			max-width: 70vw;
			padding: 30px;
			margin: 0 auto;
			position: relative;
			top: 50%;
			font-size: 30px;
			line-height: 1.5;
			transform: translateY(-50%);
			perspective: 400px;
		}

		.source {
			color: skyblue;
			margin: 0 auto;
		}
	</style>	
	<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/greensock/TweenMax.min.js"></script>
	<script type="text/javascript" src="js/splittext.js"></script>
	<script>
		$(document).ready(function($) {
			var tl = new TimelineLite, 
				mySplitText = new SplitText(".split2", {type:"words,chars"}), 
				chars = mySplitText.chars; //an array of all the divs that wrap each character

				TweenLite.set(".split2", {perspective:400});
				/*tl.add("explode", "+=2")
				//add explode effect
				numWords = mySplitText.words.length;
				for(var i = 0; i < numWords; i++){
				  tl.to(mySplitText.words[i], 0.6, {z:randomNumber(100, 500), opacity:0, rotation:randomNumber(360, 720), rotationX:randomNumber(-360, 360), rotationY:randomNumber(-360, 360)}, "explode+=" + Math.random()*0.2);
				}*/
				tl.staggerFrom(chars, 0.8, {opacity:0, scale:0, y:80, rotationX:180, transformOrigin:"0% 50% -50",  ease:Back.easeOut}, 0.02, "+=0");
			
			/*$(".bgAnimate").animate({opacity: 0,background-image: },1000,function(){
				
			});*/ 
			$('.bgAnimate').animate({opacity: 0.6},2000, function() {
				$(this)
					.css({'background-repeat': 'no-repeat','background-size': '100%','background-color':'#062047'})
					.animate({opacity: 1}); 
				
				$(".js-btn").css({opacity:1});
				$(".fullscreen-video-wrap").css({opacity:0.5});
				$(".h1fasst").css({color:"#FFFFFF"});
				//$("#load,#load-bottom").animate({opacity:0.3});
				//$("#load-bottom").css({background-color:'#b61924'});
			});
			$("#load-bottom").animate({opacity: 0},0,function(){
                var text = $(".title");
                var split = new SplitText(text);
                $(".page-home2").addClass("page-home3");
                function random(min, max){
                    return (Math.random() * (max - min)) + min;
                }
                $(split.chars).each(function(i){
                    TweenMax.from($(this), 1, {
                        opacity: 0,
                        x: random(-500, 500),
                        y: random(-500, 500),
                        z: random(-500, 500),
                        scale: .1,
                        delay: i * .06,
                        yoyo: true,
                        repeat: 0,
                        repeatDelay: 12
                    });
                });
                $(".disable-scroll").css({overflow:"hidden"});
                $(".js-btn").css({opacity:0});
                $("#load,#load-bottom").animate({opacity:0});
                $("#load,#load-bottom").css({display:"none"});
                $(".h2sp").css({padding:"0 0 0 70px"});
            });
			TweenMax.to(".h1fasst", 2, {delay:1,color:"#fdfdfd"});			
		});
		function randomNumber(min, max){
			return Math.floor(Math.random() * (1 + max - min) + min);
		}	
		function cargar_pagina(){
			$(".disable-scroll").css({"background":"#000000"});
			$('.bgAnimate').animate({opacity: 0.5},400, function() {
				
				$("body *").animate({opacity:0},400, function() {
					window.location.href = 'contact-center.php';
				});
			});
		}

	</script>
</head>
<body class="disable-scroll">
	<div id="load" class="">
		<div class="load-wrap"></div>
	</div>
	<div id="load-bottom" class="">
		<div class="load-wrap"></div>
	</div>
	<div id="page-home" class="intro section display init onc">
	
		<div class="bgAnimate"></div>
		
		<div class="fullscreen-video-wrap">
			<video id="pretzel-video" muted autoplay="" preload="auto" loop="loop">
				<source src="video/Fasst_2.mp4" type="video/mp4">
			Your browser does not support the video tag.
			</video>
		</div>

		<div class="grid-lines">
			<div class="bg">
				<div class="line line-v line-v-1"></div>
				<div class="line line-v line-v-2"></div>
				<div class="line line-v line-v-3"></div>
				<div class="line line-h line-h-1"></div>
				<div class="line line-h line-h-4"></div>
			</div>
		</div>
		<div class="wrapper gridArea">
			<img src="img/index/fasst.png" align="Fasst logo" class="logo">
			<h1 class="title">
				<span class="block">somos los pioneros en</span> <span class="block">crear el futuro</span>
			</h1>
			<a href="contact-center.php" class="btn-border js-btn link" onclick="cargar_pagina()" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">Conoce más</a>
			<div class="info">
				<p class="paragraphe">
					<span class="line"><span class="line__content" style="transform: matrix(1, 0, 0, 1, 0, 0);"><strong>FASST, S.A. DE C.V.</strong> es una empresa queretana</span></span>
					<span class="line"><span class="line__content" style="transform: matrix(1, 0, 0, 1, 0, 0);">con más de 30 años en el mercado de</span></span>
					<span class="line"><span class="line__content" style="transform: matrix(1, 0, 0, 1, 0, 0);">Tecnologías de Información y Comunicación.</span></span>
				</p>
			</div>
		</div>
	</div>
</body>
</html>
