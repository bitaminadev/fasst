<!DOCTYPE html>
<?php  
    $title = 'Servicio de geolocalización y telemetría';
	$description = 'En Fasst damos solución a las necesidades de geolocalización y telemetría de transportes y personas a través de Ubiqo y Evidence.';
	$keywords = 'fasst, ubiqo, evidence, geolocalización, telemetría';
    $author = 'Bitamina Digital';
    $lang = 'es';
    
    $page = 'geolocalizacion';
    include('commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'geolocalizacion-y-telemetria'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/geolocation-and-telemetric'.$ext;
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesGeolocalizacion.css">';

    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/geolocalizacion.html');
    include('views/modal-contacto.html');
    include('views/footer.html');
?>