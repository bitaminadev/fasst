<!DOCTYPE html>
<?php  
    $title = 'Servicios de Contact Center en México';
	$description = 'En Fasst damos soluciones de inbound y outbound a través de nuestros Contact Centers y Call Center en México. Servicios de CallFasst y Contactum.';
	$keywords = 'fasst, contact center, callfasst, contactum, call center';
    $author = 'Bitamina Digital';
    $lang = 'es';
    
    $page = 'home';
    include('commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'contact-center'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/contact-center'.$ext;
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesHome.css">';
    
    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/index.html');
    include('views/modal-contacto.html');
    include('views/footer.html');
?>