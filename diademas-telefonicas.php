<!DOCTYPE html>
<?php  
    $title = 'Diademas telefónicas profesionales para Call Center y Contact Center';
	$description = 'En Fasst damos solución de comunicación con diademas telefónicas Accutone  para la operación de Contact Center o Call Center en México y Latinoamérica.';
	$keywords = 'fasst, accutone, diademas telefónicas, call center, contact center';
    $author = 'Bitamina Digital';
    $lang = 'es';
    
    $page = 'diademas';
    include('commons/_headOpen.php');
    $url_es = $httpProtocol.$host.$url.'diademas-telefonicas'.$ext;
    $url_en = $httpProtocol.$host.$url.'en/phone-headsets'.$ext;
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesDiademas.css">';

    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/diademas.html');
    include('views/modal-contacto.html');
    include('views/footer.html');
?>