	<?php 
		if (isset($hreflang)) echo $hreflang;
		if (isset($canonical)) echo $canonical;
        echo $js;
        echo $css;
	?>
	<!-- Global site tag (gtag.js) - Google Analytics 
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-53508305-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-53508305-1');
    </script> -->
</head>

<body class="aos-init" data-aos="zoom-in" 
                data-aos-offset="200" data-aos-delay="200" 
                data-aos-duration="500"
                data-aos-easing="ease-in-out"
                data-aos-mirror="false"
                data-aos-once="false">