<?php
    $httpProtocol = 'https://';
    $host = $_SERVER['SERVER_NAME'];
    $url = '/fasst2/';
    /* $httpProtocol = 'http://';
    $host = $_SERVER['SERVER_NAME'];
    $url = '/Bitamina/fasst/'; */
    $ext = '.php';
    
    // Variables que almacenan el css y el js de la pagina
    $js = '
        <script src="'.$httpProtocol.$host.$url.'js/jquery-3.4.1.min.js"></script>
        <script src="'.$httpProtocol.$host.$url.'js/bootstrap.min.js"></script>
        <script src="'.$httpProtocol.$host.$url.'js/contacto.js"></script>
        
    ';
    $css = '
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/webfonts/all.min.css">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/bootstrap.min.css">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/styles.css">
        <link href="https://fonts.googleapis.com/css?family=Teko:300,500,600,700" rel="stylesheet">
    ';
    $hreflang = '<link rel="alternate" hreflang="x-default" href="'.$httpProtocol.$host.$_SERVER["REQUEST_URI"].'">';
?>
<html prefix="og: http://ogp.me/ns#" lang="<?php echo $lang; ?>">
<head>
    <title><?php echo $title?></title>
    <meta charset="UTF-8">
    <meta name="description" content="<?php echo $description ?>">
    <meta name="keywords" content="<?php echo $keywords ?>">
    <meta name="author" content="<?php echo $author ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="icon" type="image/png" href="<?php echo $httpProtocol.$host.$url.'img/favicon/favicon.ico'?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $httpProtocol.$host.$url.'img/favicon/apple-touch-icon.png'?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $httpProtocol.$host.$url.'img/favicon/favicon-32x32.png'?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $httpProtocol.$host.$url.'img/favicon/favicon-16x16.png'?>">
    <link rel="manifest" href="<?php echo $httpProtocol.$host.$url.'img/favicon/site.webmanifest'?>">
    <link rel="mask-icon" href="<?php echo $httpProtocol.$host.$url.'img/favicon/safari-pinned-tab.svg'?>" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="theme-color" content="#ffffff">
    <!--Metas Geo-->
    <meta name="geo.region" content="MX-QUE" />
    <meta name="geo.placename" content="Quer&eacute;taro" />
    <meta name="geo.position" content="20.589103;-100.365155" />
    <meta name="ICBM" content="20.589103, -100.365155" />
    <!--Metas OG-->
    <meta property="og:locale" content="<?php echo $lang; ?>" />
    <meta property="og:title" content="<?php echo $title?>" />
    <meta property="og:description" content="<?php echo $description ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo $httpProtocol.$host.$_SERVER['REQUEST_URI'] ?>" />
    <meta property="og:image" content="<?php echo $httpProtocol.$host.$url.'img/fasst_logo_header.png'?>" />
    <meta property="og:site_name" content="Fasst it specialist"/>

    <!--Metas DC-->
    <meta content="<?php echo $title?>" NAME='DC.Title'/>
    <meta content="<?php echo $description ?>" NAME='DC.Description'/>
    <meta content="<?php echo $author ?>" NAME='DC.Creator'/>
    <meta content='Fasst it specialist' NAME='DC.Publisher'/>
    <meta content="<?php echo $httpProtocol.$host.$_SERVER['REQUEST_URI'] ?>" NAME='DC.Identifier'/>
    <meta content="<?php echo $keywords ?>" NAME='DC.keywords'/>

    <link rel="icon" href="<?php echo $httpProtocol.$host.$url?>img/favicon/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $httpProtocol.$host.$url?>img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $httpProtocol.$host.$url?>img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $httpProtocol.$host.$url?>img/favicon/favicon-16x16.png">
	<link rel="mask-icon" href="<?php echo $httpProtocol.$host.$url?>img/favicon/safari-pinned-tab.svg" color="#5bbad5">